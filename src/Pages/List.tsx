

import CardList from "../Components/CardList";

import './List.css'
import { Stack } from "@mui/material";
import Button from "@mui/material/Button";
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

import { styled, alpha } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';

import InputBase from '@mui/material/InputBase';

import SearchIcon from '@mui/icons-material/Search';
import {useState,useEffect } from 'react';
import MovieService from "../api/MovieService";


const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
}));

const List = () => {
  const [inputValue,setInputValue]=useState("")
  const [filter,setFilter] = useState<any | null>("")
  const [dir,setDir] = useState("")
  
  
    const film = {
        id: "",
        title: "",
        popularity:"",
        poster_path:""
    };
    let [movies,setMovies] = useState([film]);
    const [isLoading,setIsLoading] = useState(true);
    


    const buildFilm =(items:any)=>
    {
        let film = {
            id: items.id,
            title: items.title,
            release_date: items.release_date,
            popularity: items.popularity,
            poster_path:items.poster_path
        }
        setMovies(old =>[...old,film]) 
    }

    useEffect(()=> {
            for(var i=1;i<=50;i++){
            MovieService.getMovieSearch(i.toString(),inputValue).then(
                async (res : any)=>{
                    let movie = await res.data.results
                    movie.forEach(buildFilm)
            })
            }
        movies.splice(0,1)
        setIsLoading(false)
        console.log("inputVal:" + inputValue)
        //console.log("movies: " + JSON.stringify(movies))
        // eslint-disable-next-line
    }, [inputValue]);

    useEffect(()=>{
        if(dir==="asc" && filter==="title")
        {
            console.log("asc title")
            setMovies([...movies.sort((a,b) => {
                if (a.title < b.title)
                return -1;
            if (a.title > b.title)
                return 1;
            return 0;
            })]);
            
        }
        if(dir==="dsc" && filter==="title")
        {
            console.log("dsc title")
            setMovies([...movies.sort((a,b) => {
                if (a.title < b.title)
                return 1;
            if (a.title > b.title)
                return -1;
            return 0;
            })]);
        }
        if(dir==="asc" && filter==="popularity")
        {
            console.log("asc popularity")
            console.log("search:"+inputValue)
            setMovies([...movies.sort((a,b) => parseFloat(a.popularity) - parseFloat(b.popularity))]);
            
        }
        if(dir==="dsc" && filter==="popularity")
        {
            console.log("dsc popularity")
            
        setMovies([...movies.sort((a,b) => parseFloat(b.popularity) - parseFloat(a.popularity))]);
        }
        
    // eslint-disable-next-line
    },[movies,dir,filter]);
    

    return(
    <>
        <div className="listContent">
            
            <div className="filter">
            
            <Stack spacing={2}>
            <h1>TOP 500 MOVIES</h1>
            <Toolbar>
                    <Search sx={{ border: 1 }}>
                            <SearchIconWrapper>
                                <SearchIcon />
                            </SearchIconWrapper>
                            <StyledInputBase
                                placeholder="Search…"
                                inputProps={{ 'aria-label': 'search' }}
                                onChange={(e) => { 
                                setInputValue(e.target.value.toLowerCase())}}/>
                    </Search>
            </Toolbar>
        
                <Autocomplete
                disablePortal
                id="combo-box-demo"
                options={["popularity", "title"]}
                sx={{ width: 300 }}
                onChange={(event,value) => { 

                    setFilter(value)
    
                }}
                renderInput={(params) => <TextField {...params} label="Filter By" />} />


                <Autocomplete
                disablePortal
                id="combo-box-demo"
                options={["ascending", "descending"]}
                onChange={(event, value) => {
                    if(value==="ascending")
                    setDir("asc")
                       
                    else    
                    setDir("dsc")

                }}
                sx={{ width: 300 }}
                renderInput={(params) => <TextField {...params} label="Sort By" />} />

                <Button 
                variant="outlined"
                onClick={()=>{
                   
                    console.log("f: " + filter)
                    console.log("d: " + dir)
                }}
                >Apply Filter
                </Button>
            </Stack>
            </div>
            
            <div className="movieList">
                {!isLoading && <Stack spacing={0.5}>
                {movies.map((items) => (
                    <CardList
                        id={items.id}
                        title={items.title}
                        popularity={items.popularity}
                        poster_path={items.poster_path} />
                ))}
            </Stack>}
            </div>
            
      </div>
      </>
    );
}
export default List;