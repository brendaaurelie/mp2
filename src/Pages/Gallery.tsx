

import { useState,useEffect } from "react";
import * as React from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';

import TabPanel from '@mui/lab/TabPanel';


import MovieService from "../api/MovieService";

import MovieGallery from "../Components/MovieGallery";
import { TabList } from "@mui/lab";


function Gallery (){
    const genreMap = {
        name: "",
        id: ""
    }
    const [genreList,setGenreList] = useState([genreMap])
    const [value, setValue] = useState("28");
    const [isLoading, setLoading] =useState(true)
    
    
    const buildGenre = (item:any)=>
    {
        let genreMap = {
            name: item.name.toString(),
            id:  item.id.toString()
        }
        
        
        setGenreList(old=>[...old,genreMap])
    }

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
        console.log("im clicking " + newValue)
      };
    
    useEffect(() => {
        console.log("genrelist" +JSON.stringify(genreList))
        MovieService.getGenre()
        .then(async (res)=>{
            let genres =  await res.data.genres
            genres.forEach(buildGenre)
        })
        .catch((err)=> {
            console.log("error: "+ err)
        })
        genreList.splice(0,1)
        setLoading(false);
        // eslint-disable-next-line
    },[]);

    return(
        <div>
        {!isLoading && 
        <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={value}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <TabList 
                onChange={handleChange} 
                variant="scrollable"
                scrollButtons="auto"
                value={value}>

                {genreList.map((items)=>(
                <Tab label={items.name} value={items.id.toString()}/>
                ))}
                </TabList>
            </Box>
        
        {genreList.map((items)=>(
          <TabPanel value={items.id.toString()}>
            <MovieGallery selectedGenre={items.id}/>
          </TabPanel>
          
        ))}
    
        </TabContext>
    </Box>
    }

        </div>
    );
    }
  
export default Gallery;




