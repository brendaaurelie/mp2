
import Box from '@mui/material/Box';

import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';

import CardMedia from '@mui/material/CardMedia';

import { CardActionArea } from '@mui/material';
import MovieService from '../api/MovieService';
import { useState,useEffect } from 'react';
import { Link } from 'react-router-dom';


export default function MovieGallery({selectedGenre} : {selectedGenre:string}) {
    const [isLoading, setLoading] = useState(true)
    const [movies,setMovies] = useState([{
        id: "",
        genre_ids: [],
        poster_path:""
    }]);

    const buildFilm =(items:any)=>
    {
        for(let i in items.genre_ids)
        {
            // eslint-disable-next-line
            if(items.genre_ids[i] == selectedGenre)
            {
                let film = {
                    id: items.id,
                    genre_ids: items.genre_ids,
                    poster_path:items.poster_path
                }
                setMovies(old =>[...old,film]) 
            }
        }
    }
   
    useEffect(()=>{
            for(var i=1;i<=20;i++){
            MovieService.getAllMovies(i.toString(),"popularity","desc").then(
                async (res : any)=>{
                    let movie = await res.data.results
                    movie.forEach(buildFilm)
            })
        }
        setLoading(false)
        movies.splice(0,1)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);


  return (
    <div>  {!isLoading && 
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
      {movies.map((items) => (
                 <Grid item xs={2}>
        <Link to={`/CardFocus/${items.id}`} className="link">
                 <Card sx={{ width: 150 }}>
                         <CardActionArea>
                             <CardMedia
                             component="img"
                             height="200"
                             image={`https://image.tmdb.org/t/p/w200/${items.poster_path}`}
                             alt="no image available"
                             />     
                         </CardActionArea>
                         </Card>
            </Link>
            </Grid>
            ))}
      </Grid>
    </Box>
    }</div>
  );
}
