
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';

import Typography from '@mui/material/Typography';
import { Link } from "react-router-dom";
import './Components.css'

function Navbar() {
        return(
            <Box sx={{ flexGrow: 1 }} >
            <AppBar position="sticky" sx={{backgroundColor: "#EFF1ED"}}>
              <Toolbar>
                <Typography
                  variant="h6"
                  noWrap
                  component="div"
                  sx={{ mr: 2 }}
                >
                 <Link className='link'  
                      to="/Gallery">
                     Gallery
                </Link>
               
                </Typography>
                <Typography
                  variant="h6"
                  noWrap
                  component="div"
                  sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
                >
                  <Link className='link'
                    
                      to="/List">
                     List
                </Link>
                </Typography>
                
              </Toolbar>
            </AppBar>
          </Box>
        );
    
}


export default Navbar;
