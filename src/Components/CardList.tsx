
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { generatePath } from 'react-router-dom';
import './Components.css'
export default function CardList({ title, popularity, id, poster_path }: { title: string; id:string; poster_path:string, popularity: string | undefined }){
  
  useEffect(()=>{
    generatePath(`/CardFocus/${id}`)
  },[id]);

  return (
    <Link to={`/CardFocus/${id}`} className="link">
    <Card sx={{ display: 'flex' }} className="card">
      <Box sx={{ display: 'flex', flexDirection: 'column'}}>
        <CardContent  sx={{ flex: '1 0 auto', width:321 }}>
          <Typography component="div" variant="h5">
            {title}
          </Typography>
          <Typography variant="subtitle1" color="text.secondary" component="div">
            {"Popularity: " + popularity}
          </Typography>

        </CardContent>
       
      </Box>
      <CardMedia
        component="img"
        sx={{ width: 151 }}
        image={`https://image.tmdb.org/t/p/w200/${poster_path}`}
        alt="no image :("
      />

    </Card>
    </Link>
  );
}
