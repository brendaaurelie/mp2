
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { IconButton } from '@mui/material';
import { useState,useEffect } from 'react';
import './Components.css'
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import MovieService from '../api/MovieService';




export default function CardFocus() {
   //const [currIdx,setCurrIdx] = useState(0)
   
  const navigate = useNavigate();
    const [movie,setMovie] = useState({
        id: "",
        genre_ids: "",
        original_title: "",
        release_date: "",
        popularity:"",
        overview:"",
        vote_count: "",
        poster_path:"",
    });

    const [movieIDs,setMovieIDs] = useState([""]);
   
    const { id } = useParams();


    useEffect(()=>{

    MovieService.getMovieDetails(id).then((res)=>{
    let movieFocus =res.data
    let film = {
        id: id || '',
        genre_ids: movieFocus.genre_ids,
        original_title: movieFocus.original_title,
        release_date: movieFocus.release_date,
        popularity:movieFocus.popularity,
        overview: movieFocus.overview,
        vote_count: movieFocus.vote_count,
        poster_path:movieFocus.poster_path
    }
        setMovie(film)
    })
  // eslint-disable-next-line react-hooks/exhaustive-deps
},[id])
const buildFilmID =(items:any)=>
    {
       
        setMovieIDs(old =>[...old,items.id]) 
    }

useEffect(()=>{
    for(var i=1;i<=20;i++){
        MovieService.getAllMovies(i.toString(),"popularity","desc").then(
            async (res : any)=>{
                let movie = await res.data.results
                movie.forEach(buildFilmID)
        })
    }
},[])

const prev = () => {
    let idx = 0
    for(const i in movieIDs)
    {
         // eslint-disable-next-line
        if(movieIDs[i] == id)
        {
            idx = parseInt(i)
        }
    }
    // eslint-disable-next-line
    if(idx == 1)
    {
       idx = movieIDs.length
    }
    let currID =  movieIDs[idx-1]
    navigate(`/CardFocus/${currID}`)   
}


const next = () => {
    let idx = 0
    for(const i in movieIDs)
    {
         // eslint-disable-next-line
        if(movieIDs[i] == id)
        {
            idx = parseInt(i)
        }
    }
    // eslint-disable-next-line
    if(idx == movieIDs.length-1)
    {
       idx = 0
    }
    let currID =  movieIDs[idx+1]
    navigate(`/CardFocus/${currID}`)
}



  return (
    <div className='layout'>
         <div className='cardFocus'>
    <Card sx={{ maxWidth: 500, maxHeight:1200 }}>
      <CardMedia
        component="img"
        height="300"
        image={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`}
        alt="no image available"
      />

      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {movie.original_title}
        </Typography>
        <Typography variant="body2" color="text.secondary" sx={{height:230}}>
          {movie.overview}
          <br/>
          Release Date: {movie.release_date}
          <br/>
          Popularity: {movie.popularity}
          <br/>
          Vote: {movie.vote_count}
        </Typography>
      </CardContent>

      <CardActions className='cardAction'>

        <IconButton size="medium" onClick={prev}>
            <ChevronLeftIcon/>
            </IconButton>
        
       
        <IconButton size="medium" onClick={next}>
            <ChevronRightIcon/>
        </IconButton>
       

      </CardActions>
    </Card>
    </div>
    </div>
  );
}
