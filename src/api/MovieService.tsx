
import axios from "axios"

const apiKey = "a3b145522df47b580bc2633548c7f133";
const baseURL = "https://api.themoviedb.org/3/";
const language = "en-US"


class MovieService {

    getGenre(){
    return axios.get(baseURL+"genre/movie/list?api_key="+apiKey+"&language=" +language);
    }

    getAllMovies(page: string, filter:string, dir: string){
        return axios.get(baseURL+"discover/movie?api_key="+apiKey+"&language="+language+"&sort_by="+filter+"."+dir+"&include_adult=false&include_video=false&page="+page+"&with_watch_monetization_types=flatrate");
    }

    getMovieSearch(page: string,search:string){
        return axios.get(baseURL+"search/movie?api_key="+apiKey+"&language="+language+"&query="+search+"&page="+page+"&include_adult=false")
    }

    getMovieDetails(id:string | undefined) {
        return axios.get(baseURL+"movie/"+id+"?api_key="+apiKey+"&language="+language)
    }

    
}


export default new MovieService()