
import './App.css';
import Navbar from './Components/Navbar';
import Gallery from './Pages/Gallery';
import List from './Pages/List';
import CardFocus from './Components/CardFocus';
import { Routes,Route } from 'react-router-dom';

function App() {
  return (
    
      <><Navbar/>
      <Routes>
      <Route path="/" element={<List/>} />
      <Route path="/Gallery" element={<Gallery />} />
      <Route path="/List" element={<List />} />
      <Route path="/CardFocus/:id" element={<CardFocus/>} />
      
      </Routes>
      
      </>
        
  );
}

export default App;
